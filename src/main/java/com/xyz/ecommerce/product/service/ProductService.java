package com.xyz.ecommerce.product.service;

import com.xyz.ecommerce.product.persistence.Product;
import com.xyz.ecommerce.product.persistence.ProductFilter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService {
    Mono<Product> findProduct(String id);
    Flux<Product> searchProduct(ProductFilter filter);
    Mono<Product> createProduct(Product product);
}
