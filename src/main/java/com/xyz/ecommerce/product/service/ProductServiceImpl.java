package com.xyz.ecommerce.product.service;

import com.xyz.ecommerce.product.persistence.Product;
import com.xyz.ecommerce.product.persistence.ProductFilter;
import com.xyz.ecommerce.product.repo.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
public class ProductServiceImpl implements ProductService{
    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);
    @Autowired
    private ProductRepo productRepo;

    @Autowired
    ReactiveMongoTemplate template;

    @GetMapping(
            value    = "/product/{productId}",
            produces = "application/json")
    @Override
    public Mono<Product> findProduct(String id) {
        return productRepo.findById(id);
    }

    @PostMapping(
            value    = "/product/search",
            produces = "application/json")
    @Override
    public Flux<Product> searchProduct(@RequestBody ProductFilter filter) {
        Query query = new Query();
        if (!StringUtils.isEmpty(filter.getName())){
            TextCriteria textCriteria = TextCriteria.forDefaultLanguage()
                    .caseSensitive(false)
                    .matchingAny(filter.getName().split(" "));
            query.addCriteria(textCriteria);
        }
        if (filter.getPriceFrom()!=null){
            query.addCriteria(new Criteria("actualPrice").gte(filter.getPriceFrom()));
        }
        if (filter.getPriceTo()!=null){
            query.addCriteria(new Criteria("actualPrice").gte(filter.getPriceTo()));
        }
        if (!CollectionUtils.isEmpty(filter.getVariations())){
            query.addCriteria(new Criteria("variations").in(filter.getVariations()));
        }
        if (!CollectionUtils.isEmpty(filter.getVariations())){
            query.addCriteria(new Criteria("branches").in(filter.getBranches()));
        }
        query.with(new PageRequest(filter.getPage()-1, filter.getPageSize()));

        return template.find(query, Product.class);
    }


    @PostMapping(
            value    = "/product",
            produces = "application/json")
    @Override
    public Mono<Product> createProduct(@RequestBody @Valid Product product) {
       return productRepo.save(product).log();
    }
}
