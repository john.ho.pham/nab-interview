package com.xyz.ecommerce.product.repo;

import com.xyz.ecommerce.product.persistence.Product;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ProductRepo extends ReactiveCrudRepository<Product, String> {
}
