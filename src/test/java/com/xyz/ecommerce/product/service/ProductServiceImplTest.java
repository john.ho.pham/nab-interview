package com.xyz.ecommerce.product.service;

import com.xyz.ecommerce.product.persistence.Product;
import com.xyz.ecommerce.product.persistence.ProductFilter;
import com.xyz.ecommerce.product.repo.ProductRepo;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=RANDOM_PORT)
public class ProductServiceImplTest {

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private WebTestClient client;

    private Set<Product> productList = new HashSet<>();

    @Test
    public void searchProduct() {
        Product product1 = new Product();
        product1.setName("IPhone 11");
        product1.setOriginalPrice(1200.);
        product1.setActualPrice(1100.);
        product1.setBranches(Arrays.asList("1"));
        product1.setVariations(Arrays.asList("4"));
        product1.setCurrency("USD");
        Product returnProduct1 = productRepo.save(product1).block();
        productList.add(returnProduct1);

        Product product2 = new Product();
        product2.setName("Samsung Note");
        product2.setOriginalPrice(1000.);
        product2.setActualPrice(900.);
        product2.setBranches(Arrays.asList("2"));
        product2.setVariations(Arrays.asList("4"));
        product2.setCurrency("USD");
        Product returnProduct2 = productRepo.save(product2).block();
        productList.add(returnProduct2);

        ProductFilter productFilter = new ProductFilter();
        productFilter.setPriceFrom(1.);
        productFilter.setPage(1);
        productFilter.setPageSize(2);
        client.post()
                .uri("/product/search")
                .body(Mono.just(productFilter), ProductFilter.class)
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isEqualTo(200)
                .expectBodyList(Product.class)
                .hasSize(2);

        productFilter = new ProductFilter();
        productFilter.setName("iphone");
        productFilter.setPage(1);
        productFilter.setPageSize(2);
        client.post()
                .uri("/product/search")
                .body(Mono.just(productFilter), ProductFilter.class)
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isEqualTo(200)
                .expectBodyList(Product.class);
    }

    @After
    public void tearDown(){
        productRepo.deleteAll(productList);
    }

    @Test
    public void createProduct200() {
        Product product = new Product();
        product.setName("IPhone 11");
        product.setOriginalPrice(1200.);
        product.setActualPrice(1100.);
        product.setBranches(Arrays.asList("1","2","3"));
        product.setVariations(Arrays.asList("4","5","6"));
        product.setCurrency("USD");
        EntityExchangeResult<Product> productEntityExchangeResult = client.post()
                .uri("/product")
                .body(Mono.just(product), Product.class)
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isEqualTo(200)
                .expectBody(Product.class)
                .returnResult();
        Product responseBody = productEntityExchangeResult.getResponseBody();
        assertEquals(product.getOriginalPrice(), responseBody.getOriginalPrice());
        assertEquals(product.getActualPrice(), responseBody.getActualPrice());
        assertEquals(product.getBranches(), responseBody.getBranches());
        assertEquals(product.getVariations(), responseBody.getVariations());
        productList.add(responseBody);
    }

    @Test
    public void createProduct400_EmptyName() {
        Product product = new Product();
        product.setOriginalPrice(1200.);
        product.setActualPrice(1100.);
        product.setBranches(Arrays.asList("1","2","3"));
        product.setVariations(Arrays.asList("4","5","6"));
        product.setCurrency("USD");
        client.post()
                .uri("/product")
                .body(Mono.just(product), Product.class)
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isEqualTo(400);
    }

    @Test
    public void createProduct400_NullPrice() {
        Product product = new Product();
        product.setOriginalPrice(1200.);
        product.setActualPrice(1100.);
        product.setBranches(Arrays.asList("1","2","3"));
        product.setVariations(Arrays.asList("4","5","6"));
        product.setCurrency("USD");
        client.post()
                .uri("/product")
                .body(Mono.just(product), Product.class)
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isEqualTo(400);
    }
}