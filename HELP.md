# Getting Started

### Reference Documentation
Product service is built on top SpringBoot & Spring Webflux. This service comprises APIs which are creating and searching products.
API definition as follows:
#####Create Product (POST /product) 
| Field       | Type     | Required     |
| :------------- | :----------: | -----------: |
|  name | string   | Yes    |
| actualPrice   | double | Yes |
| currency   | string | Yes |
| originalPrice   | double | No |
| branches   | string array | No |
| variation   | string array | No |

#####Search Product (POST /product/search) 
| Field       | Type     
| :------------- | :----------: |
|  name | string   |
| priceFrom   |double 
| priceTo  |double 
| branches   | string array |
| variation   | string array | 
| page   | number | 
| pageSize   | number | 
### Guides
#####Prerequisite
 - Java 8+
 - Docker , docker-compose (https://docs.docker.com/compose/install/)
#####How to run
- Location product-service directory then run `docker-compose up -d`
- Using these `curl` commands to verify result:
> curl -i -X POST -H "Content-Type: application/json" -d '{"name":"IPhone 11","actualPrice":1000.0, "currency":"USD"}' http://localhost:8080/product
  
>curl -i -X POST -H "Content-Type: application/json" -d '{"name":"IPhone", "page":1, "pageSize":10}' http://localhost:8080/product/search 



